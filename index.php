<?php

//autoload de composer
require 'vendor/autoload.php';
session_start();

$loader = new \Twig\Loader\FilesystemLoader('./vistas');
$twig = new \Twig\Environment($loader, []);

$productos = [
    1 => [
        'id' => 1,
        'nombre' => 'Botas de Montaña y Trekking',
        'descripcion' => 'Etiam pretium iaculis justo.',
        'precio' => 49,
        'imagen' => "Zapatos (1).png",
    ],
    2 => [
        'id' => 2,
        'nombre' => 'Zapatillas de pádel hombre',
        'descripcion' => 'Sed accumsan felis.',
        'precio' => 32,
        'imagen' => "Zapatos (2).png",
    ],
    3 => [
        'id' => 3,
        'nombre' => 'Botas Caza Solognac',
        'descripcion' => 'Integer a nibh. In quis justo.',
        'precio' => 45,
        'imagen' => "Zapatos (3).png",
    ],
    4 => [
        'id' => 4,
        'nombre' => 'Botas senderismo naturaleza',
        'descripcion' => 'Cras in purus eu magna vulputate luctus.',
        'precio' => 16,
        'imagen' => "Zapatos (4).png",
    ],
];

$_SESSION["productos"] = $productos;

$template = $twig->load('index.html');
echo $template->render(['productos' => $productos]);