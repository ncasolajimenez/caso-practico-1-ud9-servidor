<?php

//autoload de composer
require 'vendor/autoload.php';
session_start();

$loader = new \Twig\Loader\FilesystemLoader('./vistas');
$twig = new \Twig\Environment($loader, []);

$id_producto = $_GET["id_producto"];

$template = $twig->load('producto.html');
$producto = $_SESSION["productos"][$id_producto];
echo $template->render(['producto' => $producto]);
