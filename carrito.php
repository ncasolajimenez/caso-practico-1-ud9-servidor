<?php

//autoload de composer
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

require 'vendor/autoload.php';
session_start();

$loader = new FilesystemLoader('./vistas');
$twig = new Environment($loader, []);

$productos = [];

if(!isset($_COOKIE["productos_carrito"])) {
    setcookie("productos_carrito", json_encode([]), time() + (86400 * 30), "/"); // 86400 = 1 day
} else {
    $productos = json_decode($_COOKIE["productos_carrito"], true);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_POST["_metodo"] === "PUT") {
    // Peticion de Añadir al Carrito
    $id_producto = $_POST["id"];
    if(isset($productos[$id_producto])) {
        // Si ya existe sumo la cantidad
        $productos[$id_producto]["cantidad"] += $_POST["cantidad"];
    } else {
        // Sino existe lo añado al carrito
        $producto = $_SESSION["productos"][$id_producto];
        $producto["cantidad"] = $_POST["cantidad"];
        $productos[$id_producto] = $producto;
    }
}
if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_POST["_metodo"] === "DELETE") {
    $id_producto = $_POST["id"];
    unset($productos[$id_producto]);
}

setcookie("productos_carrito", json_encode($productos), time() + (86400 * 30), "/"); // 86400 = 1 day
$template = $twig->load('carrito.html');
echo $template->render(['productos' => $productos]);
